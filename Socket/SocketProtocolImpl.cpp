#include "pch.h"
#define DLL_API _declspec(dllexport)
#include "SocketProtocol.h"


SocketProtocol::SocketProtocol()
{
  this->cache = nullptr;
  this->len = 0;
}

SocketProtocol::SocketProtocol(const SocketProtocol& other)
{
  this->len = other.len;
  this->cache = new char[this->len];
}

SocketProtocol::~SocketProtocol()
{
  if (nullptr != this->cache) {
    delete[] this->cache;
    this->cache = nullptr;
  }
  this->len = 0;
}

int SocketProtocol::Init()
{
  this->cache = nullptr;
  this->len = 0;
  return 0;
}

int SocketProtocol::Send(const char* data, const int len)
{
  if (nullptr == data)
    return -1;

  if (0 >= len)
    return -2;

  this->len = len + 1;
  this->cache = new char[this->len];
  memcpy_s(this->cache, this->len, data, len);
  return 0;
}

int SocketProtocol::Receive(char* data, int& len)
{
  if (nullptr == data)
    return -1;

  len = this->len - 1;
  memcpy(data, this->cache, this->len - 1);
  return 0;
}

int SocketProtocol::Destroy()
{
  if (nullptr != this->cache) {
    delete[] this->cache;
    this->cache = nullptr;
  }
  this->len = 0;
  return 0;
}
