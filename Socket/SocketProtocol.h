#pragma once
#include "pch.h"

#ifndef DLL_API
#define DLL_API _declspec(dllimport)
#endif // !DLL_API

class DLL_API SocketProtocol
{

private:
  char* cache;
  int len;

public:
  SocketProtocol();
  SocketProtocol(const SocketProtocol& other);
  ~SocketProtocol();

public:
  // 初始化
  virtual int Init();
  // 发送数据
  virtual int Send(const char* data, const int len);
  // 接受数据
  virtual int Receive(char* data, int& len);
  // 释放资源
  virtual int Destroy();

public:
  SocketProtocol& operator= (const SocketProtocol& other)
  {
    this->len = other.len;
    this->cache = new char[this->len];
    return *this;
  }

};

