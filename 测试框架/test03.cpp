#include <iostream>
#include "Encrypt.h"

using namespace std;

int main_02()
{

  char data[512] = { 0 };
  const int len = 10;
  memcpy(data, "1234567890aabbcc", len);
  
  char receive[512] = { 0 };
  int recLen = 0;

  char encData[512] = { 0 };
  int encLen = 0;
  

  Encrypt* enc = new Encrypt();

  int ret;

  ret = enc->encode(data, len, encData, encLen);
  if (0 != ret) {
    printf("enc->encode() err: %d \n ", ret);
    goto End;
  }
  else {
    printf("加密前: %s, 加密后: %s \n", data, encData);
  }


  ret = enc->decode(encData, encLen, receive, recLen);
  if (0 != ret) {
    printf("sp->Send() err: %d \n ", ret);
    goto End;
  }
  else {
    printf("解密前: %s, 解密后: %s \n", encData, receive);
  }


End:
  delete enc;

  return 0;

}