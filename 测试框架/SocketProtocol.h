#pragma once

class SocketProtocol
{
public:
  // 初始化
  virtual int Init();
  // 发送数据
  virtual int Send(const char* data, const int len);
  // 接受数据
  virtual int Receive(char* data, int& len);
  // 释放资源
  virtual int Destroy();
};