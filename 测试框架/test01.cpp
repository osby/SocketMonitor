#include <iostream>
#include "SocketProtocol.h"

using namespace std;

int main_01()
{

  char data[512] = { 0 };
  const int len = 10;
  memcpy(data, "1234567890aabbcc", len);
  
  char receive[512] = { 0 };
  int recLen = 0;
  

  SocketProtocol* sp = new SocketProtocol();

  int ret;
  ret = sp->Init();
  if (0 != ret) {
    printf("sp->Init() err: %d \n ", ret);
    goto End;
  }

  ret = sp->Send(data, len);
  if (0 != ret) {
    printf("sp->Send() err: %d \n ", ret);
    goto End;
  }

  ret = sp->Receive(receive, recLen);
  if (0 != ret) {
    printf("sp->Send() err: %d \n ", ret);
    goto End;
  }
  else {
    printf("收到数据: %s, 长度: %d \n", receive, recLen);
  }

  ret = sp->Destroy();
  if (0 != ret) {
    printf("sp->Destroy() err: %d \n ", ret);
    goto End;
  }

End:
  delete sp;

  return 0;

}