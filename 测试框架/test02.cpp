#include <iostream>
#include "Encrypt.h"
#include "SocketProtocol.h"

using namespace std;

int main()
{
  char data[512] = { 0 };
  const int len = 10;
  memcpy(data, "1234567890aabbcc", len);
  
  char receive[512] = { 0 };
  int recLen = 0;

  char encData[512] = { 0 };
  int encLen = 0;
  

  Encrypt* enc = new Encrypt();
  SocketProtocol* sp = new SocketProtocol();

  int ret;
  ret = enc->encode(data, len, encData, encLen);
  if (0 != ret) {
    printf("enc->encode() err: %d \n ", ret);
    goto End;
  }
  else {
    printf("加密前: %s, 加密后: %s \n", data, encData);
  }


  ret = sp->Init();
  if (0 != ret) {
    printf("->Init() err: %d \n ", ret);
    goto End;
  }




  ret = enc->decode(encData, encLen, receive, recLen);
  if (0 != ret) {
    printf("sp->Send() err: %d \n ", ret);
    goto End;
  }
  else {
    printf("解密前: %s, 解密后: %s \n", encData, receive);
  }


End:
  delete enc;
  delete sp; // 同时调用两个本地 .dll , 这条语句会报错

  return 0;

}