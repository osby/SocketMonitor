#pragma once
#include "pch.h"

#ifndef DLL_API_ENCRYPT
#define DLL_API_ENCRYPT _declspec(dllimport)
#endif // !DLL_API


class DLL_API_ENCRYPT Encrypt
{
public:
  // ����
  virtual int encode(const char* plainData, const int plainLen, char* encryptData, int& encriptLen);
  // ����
  virtual int decode(const char* encryptData, const int encriptLen, char* plainData, int& plainLen);
};

