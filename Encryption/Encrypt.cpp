#include "pch.h"
#define DLL_API_ENCRYPT _declspec(dllexport)
#include "Encrypt.h"

int Encrypt::encode(const char* plainData, const int plainLen, char* encryptData, int& encriptLen)
{
  if (nullptr == plainData)
    return -1;

  if (0 >= plainLen)
    return -2;

  if (nullptr == encryptData)
    return -3;

  encriptLen = plainLen;
  for (int i = 0; i < plainLen; i++) {
    encryptData[plainLen - i - 1] = plainData[i];
  }

  return 0;
}

int Encrypt::decode(const char* encryptData, const int encriptLen, char* plainData, int& plainLen)
{
  if (nullptr == encryptData)
    return -1;

  if (0 >= encriptLen)
    return -2;

  if (nullptr == plainData)
    return -3;

  plainLen = encriptLen;
  for (int i = 0; i < encriptLen; i++) {
    plainData[plainLen - i - 1] = encryptData[i];
  }

  return 0;
}
